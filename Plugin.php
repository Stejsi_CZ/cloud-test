<?php namespace Softresource\DockerTest;

use Softresource\DockerTest\Components\Images;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            Images::class => 'imagesComponent'
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'Základní nastavení',
                'description' => '',
                'category'    => 'Docker test - Images',
                'icon'        => 'oc-icon-openid',
                'class'       => 'Softresource\DockerTest\Models\Settings',
                'order'       => 500,
                'keywords'    => 'dt'
            ]
        ];
    }
}
