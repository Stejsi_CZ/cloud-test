<?php namespace Softresource\DockerTest\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSoftresourceDockertestImages extends Migration
{
    public function up()
    {
        Schema::create('softresource_dockertest_images', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('name');
            $table->integer('position');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('softresource_dockertest_images');
    }
}
