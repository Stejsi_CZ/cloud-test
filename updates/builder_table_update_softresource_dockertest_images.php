<?php namespace Softresource\DockerTest\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSoftresourceDockertestImages extends Migration
{
    public function up()
    {
        Schema::table('softresource_dockertest_images', function($table)
        {
            $table->string('url');
        });
    }
    
    public function down()
    {
        Schema::table('softresource_dockertest_images', function($table)
        {
            $table->dropColumn('url');
        });
    }
}
