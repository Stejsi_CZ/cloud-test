<?php namespace Softresource\DockerTest\Models;

use Model;

class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'softresource_dockertest_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';
}
