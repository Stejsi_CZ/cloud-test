<?php namespace Softresource\DockerTest\Components;

use Cms\Classes\ComponentBase;
use Softresource\DockerTest\Models\Image;

class Images extends ComponentBase
{
    /**
     * @var array
     */
    public $books;

    public function componentDetails()
    {
        return [
            'name'        => 'Images',
            'description' => 'Přehled všech vygenerovaných obrázků'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function init()
    {
        parent::init();
    }

    public function onRun()
    {
        $this->page['images'] = Image::all();
    }
}
