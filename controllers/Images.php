<?php namespace Softresource\DockerTest\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Faker\Factory;
use Softresource\DockerTest\Models\Image;
use Softresource\DockerTest\Models\Settings;

class Images extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController'    ];

    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Softresource.DockerTest', 'main-menu-item');
    }

    public function onCreateImages(){
        $faker = Factory::create();

        \Db::table('softresource_dockertest_images')->truncate();
        \Storage::deleteDirectory("uploads/public/generate");
        \Storage::makeDirectory("uploads/public/generate");

        $countLimit = Settings::get('count_generate') ? Settings::get('count_generate'): 10;

        for ($i = 0; $i < $countLimit; $i++){
          Image::create([
                 'name' => "Obrázek ".$i,
                 'position' => $i,
                 'url' => $faker->image(storage_path("app/uploads/public/generate"),300, 300, null, false)
             ]);
        }

        return $this->listRefresh();
    }
}
